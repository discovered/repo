<%@ page 
  session="false"
  contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"

  import="java.io.*"
  import="java.util.*"
  import="java.net.*"

  import="org.apache.nutch.html.Entities"
  import="org.apache.nutch.metadata.Nutch"
  import="org.apache.nutch.searcher.*"
  import="org.apache.nutch.plugin.*"
  import="org.apache.nutch.clustering.*"
  import="org.apache.hadoop.conf.*"
  import="org.apache.nutch.util.NutchConfiguration"

  import="org.creativecommons.learn.RdfStoreFactory"
  import="org.creativecommons.learn.oercloud.*"

%>
<%
  String language =
    ResourceBundle.getBundle("org.nutch.jsp.search", request.getLocale())
    .getLocale().getLanguage();
  String requestURI = HttpUtils.getRequestURL(request).toString();
  String base = requestURI.substring(0, requestURI.lastIndexOf('/'));
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%
  // To prevent the character encoding declared with 'contentType' page
  // directive from being overriden by JSTL (apache i18n), we freeze it
  // by flushing the output buffer. 
  // see http://java.sun.com/developer/technicalArticles/Intl/MultilingualJSP/
  out.flush();
%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/i18n" prefix="i18n" %>
<i18n:bundle baseName="org.nutch.jsp.search"/>
<html lang="<%= language %>">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<head>
<title>DiscoverEd: Feeds</title>
<link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
<jsp:include page="/include/style.html"/>
</head>

<body>
<jsp:include page="<%= \"../\" + language + \"/include/header.html\"%>"/>

<div class="box">

<% Collection<Feed> feeds = null;

	if (request.getParameter("c") != null) { 
    	Curator c = RdfStoreFactory.get().forDEd().load(Curator.class, request.getParameter("c")); 
    	feeds = c.getFeeds(); 
    %>
<h1>Feeds for <%=c.getName() %></h1>
<% } else { 
		feeds = RdfStoreFactory.get().forDEd().loadDeep(Feed.class);%>
<h1>Feeds</h1>
<% } %>

<ul>
	<% for (Feed f : feeds) { %>
	<li><%=f.getUri().toString() %> (<%=f.getCurator().getName()%>)
	<ul><li>Last aggregated on: <%=f.getLastImport().toString() %></li></ul>
	</li>
	<% } %>
</ul>

</div>

<jsp:include page="/footer.jsp"/>

</body>
</html>
