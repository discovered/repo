package org.creativecommons.learn.aggregate.feed;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.creativecommons.learn.DiscoverEdTestCase;

public class TestMediaWikiCategory extends DiscoverEdTestCase {
	public void testGetCategoryMembers() {
		MediaWikiCategory mwc = new MediaWikiCategory();
		Set<String> pages = new HashSet<String>(mwc.allPagesInCategory("http://wiki.creativecommons.org/api.php", "Category:DiscoverEd"));
		assertTrue(pages.contains("DiscoverEd Quickstart"));
	}
	
	public void testLotsOfGetCategoryMembers() {
		MediaWikiCategory mwc = new MediaWikiCategory();
		Set<String> pages = new HashSet<String>(mwc.allPagesInCategory("http://en.wikipedia.org/w/api.php", "Category:Biology"));
		assertTrue(pages.size() > 100);
	}
	
	public void testCalculateApiUrlWithCreativeCommonsWiki() {
		MediaWikiCategory mwc = new MediaWikiCategory();
		URL apiURL =  mwc.calculateApiUrl("http://wiki.creativecommons.org/Category:DiscoverEd");
		assertEquals("http://wiki.creativecommons.org/api.php", apiURL.toString());
	}
	
	public void testCalculateApiUrlWithEnglishWikipedia() {
		MediaWikiCategory mwc = new MediaWikiCategory();
		URL apiURL =  mwc.calculateApiUrl("http://en.wikipedia.org/wiki/Category:People");
		assertEquals("http://en.wikipedia.org/w/api.php", apiURL.toString());
	}
	
	public void testGetPageURL() throws MalformedURLException, URISyntaxException {
		/* MediaWiki URLs are kind of their own idiomatic mess. Here we test that,
		 * to the best of our knowledge, we succeed in the same way.
		 */
		URL prettyCategoryURL = new URL("http://wiki.creativecommons.org/Category:Sample_pages");
		URL uglyCategoryURL = new URL("http://wiki.creativecommons.org/index.php?title=Category:Sample_pages");
		URL prettyPageURL = new URL("http://wiki.creativecommons.org/Some_page_with/without_slashes");
		URL uglyPageURL = new URL("http://wiki.creativecommons.org/index.php?title=Some_page_with%2Fwithout_slashes");
		URL prettyResult;
		URL uglyResult;
		try {
			prettyResult = MediaWikiCategory.createPageURL(prettyCategoryURL, "Some page with/without slashes");
			uglyResult = MediaWikiCategory.createPageURL(uglyCategoryURL, "Some page with/without slashes");
		} catch (MalformedURLException e) {
			throw new RuntimeException(e); // and then the test will Error out.
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e); // and then the test will Error out.
		}
		assertEquals(prettyPageURL, prettyResult);
		assertEquals(uglyPageURL, uglyResult);
	}

}
