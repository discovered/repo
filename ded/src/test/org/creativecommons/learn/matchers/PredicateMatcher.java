package org.creativecommons.learn.matchers;

import org.creativecommons.learn.IndexFieldName;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class PredicateMatcher extends TypeSafeMatcher<String> {
           
       private String collapsed;
       private String predicateURI;

      public PredicateMatcher(String predicateURI) {
           this.collapsed = IndexFieldName.collapseResource(predicateURI);
               this.predicateURI = predicateURI;
       }
               
       @Override
       public boolean matchesSafely(String item) {
               return item.endsWith("_" + collapsed);
       }

       @Override
       public void describeTo(Description description) {
               description.appendText("a collapsed predicate relating to ").appendValue(this.predicateURI);
       }
               
}