package org.creativecommons.learn;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.creativecommons.learn.RdfStore;

import de.fuberlin.wiwiss.ng4j.NamedGraph;

public class TestGetAllRdfStores extends DiscoverEdTestCase {

	public static void test() throws SQLException {
		RdfStore store = RdfStoreFactory.get().forDEd();
		Collection<String> got = RdfStoreFactory.get().getCollectionOfProvenanceGraphsURIs();
		Iterator<String> it = got.iterator();
		assertEquals(RdfStoreFactory.SITE_CONFIG_URI, it.next());
		assertFalse(it.hasNext());
        store.close();
	}

	public static void testWorksTheSecondTime() throws SQLException {
		RdfStore store = RdfStoreFactory.get().forDEd();
		RdfStore the_same_store = RdfStoreFactory.get().forDEd();
		Iterator<String> it = RdfStoreFactory.get().getCollectionOfProvenanceGraphsURIs().iterator();
		assertEquals(RdfStoreFactory.SITE_CONFIG_URI, it.next());
		assertFalse(it.hasNext());
        store.close();
        the_same_store.close();
	}

}
