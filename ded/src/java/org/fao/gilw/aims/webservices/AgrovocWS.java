/**
 * AgrovocWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.3 Oct 05, 2005 (05:23:37 EDT) WSDL2Java emitter
 *updated by jeetendra.singh@fao.org / Oct 2009.
 */

package org.fao.gilw.aims.webservices;

public interface AgrovocWS extends java.rmi.Remote {
    public java.lang.String[] getDefinitions(int termcode, java.lang.String languagecode) throws java.rmi.RemoteException;
    public java.lang.String getAgrovocLanguages() throws java.rmi.RemoteException;
    public java.lang.String getTermcodeByTerm(java.lang.String searchTerm) throws java.rmi.RemoteException;
    public java.lang.String getTermcodeByTermXML(java.lang.String searchTerm, java.lang.String format) throws java.rmi.RemoteException;
    public java.lang.String getTermByLanguage(int termcode, java.lang.String language) throws java.rmi.RemoteException;
    public java.lang.String getTermByLanguageXML(int termcode, java.lang.String language, java.lang.String format) throws java.rmi.RemoteException;
    public java.lang.String getTermsListByLanguage(java.lang.String termcodes_list, java.lang.String language) throws java.rmi.RemoteException;
    public java.lang.String getTermsListByLanguage2(java.lang.String termcodes_list, java.lang.String language, java.lang.String separator) throws java.rmi.RemoteException;
    public java.lang.String getTermsListByLanguageXML(java.lang.String termcodes_list, java.lang.String language, java.lang.String format) throws java.rmi.RemoteException;
    public java.lang.String getAllLabelsByTermcode(int termcode) throws java.rmi.RemoteException;
    public java.lang.String getAllLabelsByTermcode2(int termcode, java.lang.String separator) throws java.rmi.RemoteException;
    public java.lang.String getAllLabelsByTermcodeXML(int termcode, java.lang.String format) throws java.rmi.RemoteException;
    public java.lang.String simpleSearchByMode(java.lang.String searchString, java.lang.String searchmode) throws java.rmi.RemoteException;
    public java.lang.String simpleSearchByMode2(java.lang.String searchString, java.lang.String searchmode, java.lang.String separator) throws java.rmi.RemoteException;
    public java.lang.String simpleSearchByModeXML(java.lang.String searchString, java.lang.String searchmode, java.lang.String format) throws java.rmi.RemoteException;
    // created by jeetendra.singh@fao.org
    public java.lang.String simpleSearchByModeandLang(java.lang.String searchString, java.lang.String searchmode, java.lang.String separator, java.lang.String language) throws java.rmi.RemoteException;
    public java.lang.String simpleSearchByModeLangXML(java.lang.String searchString, java.lang.String searchmode, java.lang.String format, java.lang.String language) throws java.rmi.RemoteException;
   
    public java.lang.String searchByTerm(java.lang.String searchString) throws java.rmi.RemoteException;
    public java.lang.String searchByTerm2(java.lang.String searchString, java.lang.String separator) throws java.rmi.RemoteException;
    public java.lang.String searchByTermXML(java.lang.String searchString, java.lang.String format) throws java.rmi.RemoteException;
    public java.lang.String searchCategoryByMode(java.lang.String searchString, java.lang.String lang, java.lang.String schemeid, java.lang.String searchmode, java.lang.String separator) throws java.rmi.RemoteException;
    public java.lang.String searchCategoryByModeXML(java.lang.String searchString, java.lang.String searchmode, java.lang.String schemeid, java.lang.String lang, java.lang.String format) throws java.rmi.RemoteException;
    public java.lang.String[] getConceptByTerm(java.lang.String searchTerm) throws java.rmi.RemoteException;
    public java.lang.String[] getConceptInfoByTermcode(java.lang.String termcodestring) throws java.rmi.RemoteException;
    public java.lang.String getConceptInfoByTermcodeXML(java.lang.String termcodestring, java.lang.String format) throws java.rmi.RemoteException;
    public java.lang.String getDefinitionsXML(int termcode, java.lang.String languagecode, java.lang.String format) throws java.rmi.RemoteException;
    public java.lang.String getTermExpansion(java.lang.String query, java.lang.String langCode) throws java.rmi.RemoteException;
    
//created by jeetendra.singh@fao.org
   public java.lang.String getStatus(int termcode, java.lang.String langcode, java.lang.String userlangcode) throws java.rmi.RemoteException;
   public java.lang.String getRelationByTermcodeXML(int termcode, java.lang.String relation, java.lang.String format) throws java.rmi.RemoteException;
   public java.lang.String complexSearchByTermXML(java.lang.String searchString, java.lang.String seprator, java.lang.String format) throws java.rmi.RemoteException;
}
