package org.creativecommons.learn.aggregate.feed;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.ElementIterator;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.ws.rs.core.UriBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.creativecommons.learn.DEdConfiguration;
import org.creativecommons.learn.RdfStore;
import org.creativecommons.learn.RdfStoreFactory;
import org.creativecommons.learn.oercloud.Feed;
import org.creativecommons.learn.oercloud.Resource;
import org.creativecommons.learn.plugin.MetadataRetrievers;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MediaWikiCategory {

	public final static Log LOG = LogFactory.getLog(MediaWikiCategory.class);

	private HttpClient client;
	private MetadataRetrievers metadataRetrievers;
	public MediaWikiCategory() {
		this.client = new HttpClient();
		this.metadataRetrievers = new MetadataRetrievers(DEdConfiguration
				.create());
	}
	
	public List<String> allPagesInCategory(String apiRoot, String categoryName) {
		ArrayList<String> ret = new ArrayList<String>();

		boolean keepGoing = true;
		String cmcontinue = null;
		
		while (keepGoing) {
			// prepare GET parameters
			NameValuePair[] args;
			if (cmcontinue == null) {
				// the initial case
				args = new NameValuePair[4];
				args[0] = new NameValuePair("action", "query");
				args[1] = new NameValuePair("list", "categorymembers");
				args[2] = new NameValuePair("cmtitle", categoryName);
				args[3] = new NameValuePair("format", "xml");
			} else {
				args = new NameValuePair[5];
				args[0] = new NameValuePair("action", "query");
				args[1] = new NameValuePair("list", "categorymembers");
				args[2] = new NameValuePair("cmtitle", categoryName);
				args[3] = new NameValuePair("format", "xml");
				args[4] = new NameValuePair("cmcontinue", cmcontinue);
			}
	
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true); // never forget this!
			DocumentBuilder builder;
			
			try {
				builder = factory.newDocumentBuilder();
			} catch (ParserConfigurationException e) {
				LOG.error("How could you really screw this up? Amazing.", e);
				return ret;
			}

			Document doc = null;
		
			// pass them off to the API
			GetMethod gm = new GetMethod(apiRoot);
			gm.setQueryString(args);
			try {
				this.client.executeMethod(gm);
				doc = builder.parse(gm.getResponseBodyAsStream());
				this.logXMLDocument(doc);
			} catch (HttpException e) {
				LOG.error("Sad, we got an error", e);
				return ret;
			} catch (IOException e) {
				LOG.error("Sad, we got an error", e);
				return ret;
			} catch (SAXException e) {
				LOG.error("Apparently MediaWiki gaves us back invalid XML. Life is strange.", e);
			} finally {
				gm.releaseConnection();
			}

			XPathFactory xpathFactory = XPathFactory.newInstance();
			XPath xpath = xpathFactory.newXPath();
			
			XPathExpression expr;
			try {
				expr = xpath.compile("//cm");
			} catch (XPathExpressionException e) {
				LOG.error("If you get this message, it means the hard-coded XPath expression in the code has invalid syntax.", e);
				return ret;
			}
			
			Object xpathResult;
			try {
				xpathResult = expr.evaluate(doc, XPathConstants.NODESET);
			} catch (XPathExpressionException e) {
				LOG.error("If you get this message, it means there is something wrong with the hard-coded XPath expression in the code.", e);
				return ret;
			}
	
			NodeList nodes = (NodeList) xpathResult;
			
			for (int i = 0; i < nodes.getLength(); i++) {
			    NamedNodeMap nodeMap = nodes.item(i).getAttributes();
			    ret.add(nodeMap.getNamedItem("title").getNodeValue());
			}
			
			// if there a cmcontinue argument? If so, use it. If not, stop looping.
			
			try {
				expr = xpath.compile("//query-continue/categorymembers[@cmcontinue]");
		 	} catch (XPathExpressionException e) {
		 		LOG.error("If you get this message, it means a hard-coded XPath expression in the code has invalid syntax. It worked once, so you should never see this.", e);
		 		return ret;
			}
		 	
		 	try {
		 		xpathResult = expr.evaluate(doc, XPathConstants.NODESET);
		 	} catch (XPathExpressionException e) {
		 		LOG.error("If it was going to blow up, it would have done so earlier.", e);
		 		return ret;
		 	}
		 	
		 	nodes = (NodeList) xpathResult;
		 	boolean foundCmcontinue = false;
		 	for (int i = 0 ; i < nodes.getLength(); i++) {
		 		NamedNodeMap nodeMap = nodes.item(i).getAttributes();
		 		cmcontinue = nodeMap.getNamedItem("cmcontinue").getNodeValue();
		 		foundCmcontinue = true;
		 		break; // only need to do this once
		 	}
		 	
		 	if (foundCmcontinue) {
		 		keepGoing = true;
		 	} else {
		 		keepGoing = false;
		 	}
		}
		
		LOG.debug("Found these pages " + ret.toString() + " for this category " + categoryName);
		
		return ret;
	}
	
	public static URL createPageURL(URL feedURL, String pageName) throws MalformedURLException, UnsupportedEncodingException, URISyntaxException {
		// Detect if the page name lives inside a query string. We have to do this because
		// if the wiki uses mod_rewrite AKA "pretty URLs", we must use a different kind of
		// escaping.
		
		String noSpacesPageName = pageName.replace(' ', '_');
		
		// calculate base URL
		boolean urlInvolvesQueryString = feedURL.toString().contains("?");
		if (urlInvolvesQueryString) {
			/* find ?title= */
			String feedURLasString = feedURL.toString();
			int startOfTitleEquals = feedURLasString.indexOf("title=");
			int endIndex = startOfTitleEquals + "title=".length();
			String everythingUpToTitleEquals = feedURLasString.substring(0, endIndex);
			return new URL(everythingUpToTitleEquals + 
					URLEncoder.encode(noSpacesPageName, "utf-8"));
		} else {
			URL templateAsURL = new URL(feedURL, "{arg}");
			URI built = UriBuilder.fromPath(templateAsURL.toString()).build(noSpacesPageName);
			return built.toURL();
		}
	}
	
	/**
	 * Take the SyndEntry "entry", and add or update a corresponding Resource in
	 * our RdfStore.
	 * 
	 * @throws URISyntaxException 
	 */
	public void poll(Feed feed) {
		/* The feed URI is the provenance of the information we find here. Pull out a data store. */
		RdfStore store = RdfStoreFactory.get().forProvenance(feed.getUri().toString());
		
		/* We're going to need:
		 *  - the path to api.php
		 *  - the wiki root, so we can append other title strings to it, and get URLs
		 *    that we can download.
		 */
		
		/* Separate the category name from the wiki base */
		URL feedURL;
		try {
			feedURL = new URL(feed.getUri().toString());
		} catch (MalformedURLException e) {
			LOG.error("I would be quite disappointed if we had a feed URI that was not a valid URL.", e);
			throw new RuntimeException(e);
		}

		// The wiki root is easy. That we get by fiddling with the URL of the category.
		calculateMediawikiPageRoot(feedURL.toString());
		
		URL apiURL = this.calculateApiUrl(feedURL.toString());
	    if(apiURL == null) {
	    	throw new IllegalStateException("Um, we could not find the MediaWiki API root.");
	    }
	    
		// calculate just the category name. Note that if the category has a slash in it, this
		// will not work. It would be more convenient if MediaWiki emitted the title in a format
		// where parsers could understand it.
		String[] thingsWithSlashes = feed.getUri().toString().split("/");
		int lastItemIndex = thingsWithSlashes.length - 1;
		String categoryName = thingsWithSlashes[lastItemIndex];
	    
	    List<String> allPagesInCategory = this.allPagesInCategory(apiURL.toString(), categoryName);
	    for (String pageName: allPagesInCategory) {
			URL pageURL;
	    	try {
				pageURL = MediaWikiCategory.createPageURL(feedURL, pageName);
			} catch (MalformedURLException e) {
				LOG.error("I can't believe it. A pageURL could not be formed by joining " + feedURL.toString() + " with " + pageName);
				throw new RuntimeException(e);
			} catch (UnsupportedEncodingException e) {
				LOG.error("it seems your system does not know what UTF-8 is. The mind boggles.", e);
				return;
			} catch (URISyntaxException e) {
				LOG.error("You have got to be kidding. Blowing up now.", e);
				throw new RuntimeException(e);
			}
			this.addPage(store, feed, pageName, pageURL);
	    }
	  }
	
	private void logXMLDocument(Document doc) {
		// Set up the output transformer
		TransformerFactory transfac = TransformerFactory.newInstance();
		Transformer trans;
		try {
			trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");

			// Print the DOM node

			StringWriter sw = new StringWriter();
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(doc);
			trans.transform(source, result);
			String xmlString = sw.toString();

			LOG.debug(xmlString);
		} catch (TransformerConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void addPage(RdfStore store, Feed feed, String pageName, URL pageURL) {
		Resource r = new Resource(URI.create(pageURL.toString()));

		// Back when SyndFeed parsed the feed, it read in from the feed
		// all of the metadata it could find for this URI. Now it has
		// made that metadata available in the object "entry".

		// In fact, this feed is one of the resource's "sources".
		// So let's add this feed to the resource's list of sources.
		r.getSources().add(feed);
		r.setTitle(pageName);

		// I don't know what the "Description" of a wiki page would be.
		
		// Is the set of Category names of a wiki page really same as dct:subject?
		// dc:category?
		// If so, we can get that data out of the MediaWiki API.
		
		// Things like author are also kind of messy. Do we want to try hard to get them?
		
		// dc:type and dc:format... let's skip those.
		
		// For dc:contributors, we could get 'em from the API.
		LOG.info("Added MediaWiki page with URI: " + r.getUri().toString());
		store.saveDeep(r);

		// Load additional metadata from external sources
		metadataRetrievers.retrieve(r);
		
		store.saveDeep(r);
	}

	public URL calculateApiUrl(String pageURI) {
		/* Okay, so now for the api.php path. Unfortunately, MediaWiki doesn't (reliably)
		 * provide metadata on the location of api.php. And we can't really infer it, since
		 * installations differ.
		 * 
		 * So we have to look through the HTML of the Category page, looking for hints as to
		 * the value of the $wgScriptPath MediaWiki variable. One way to do that is to try
		 * to find the OpenSearch script path. Then just remove the OpenSearch part, and
		 * then replace "opensearch_desc.php" with "api.php".
		 * 
		 * Since HTML pages don't always parse properly, we use the Swing HTML parser (which
		 * is fault-tolerant) rather than an XML parser, even though MediaWiki typically its output
		 * is XHTML 1.0 Transitional.
		 */
	    URL apiURL = null;
	    URL url;
		try {
			url = new URL(pageURI);
		} catch (MalformedURLException e) {
			LOG.error("It should be impossible to have an invalid URL when it was already a valid URI", e);
			return apiURL;
		}
	    HTMLEditorKit kit = new HTMLEditorKit(); 
	    HTMLDocument doc = (HTMLDocument) kit.createDefaultDocument();
	    doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
	    Reader HTMLReader;
		try {
			HTMLReader = new InputStreamReader(url.openConnection().getInputStream());
		} catch (IOException e) {
			LOG.error("We could not get the MediaWiki page, so we could not calculate the API base path.", e);
			return apiURL;
		}
	    try {
			kit.read(HTMLReader, doc, 0);
		} catch (IOException e) {
			LOG.error("We could not get the MediaWiki page, so we could not calculate the API base path.", e);
			return apiURL;
		} catch (BadLocationException e) {
			LOG.error("We somehow created an invalid URL, even though we already validated once.", e);
			return apiURL;
		} 

	    //  Get an iterator for all HTML tags.
		// I can't *believe* how terrible the Swing HTML API is, left and right.
		// It's appalling. This code sucks proportionally.
		
	    ElementIterator it = new ElementIterator(doc); 
	    Element elem = it.next();
	    URL openSearchURL = null;
	    
	    while(elem != null) {
	    	String name = elem.getName();
	    	String rel = "";
	    	String href = "";
	    	String type = "";
	    	Object relO = elem.getAttributes().getAttribute(HTML.Attribute.REL);
	    	if (relO != null) {
	    		rel = relO.toString();
	    	}
	    	
	    	Object typeO = elem.getAttributes().getAttribute(HTML.Attribute.TYPE);
	    	if (typeO != null) {
	    		typeO = type.toString();
	    	}
	    	
	    	Object hrefO = elem.getAttributes().getAttribute(HTML.Attribute.HREF);
	    	if (hrefO != null) {
	    		href = hrefO.toString();
	    	}
	    	
	    	if ( (name.equals("link")) &&
	    		 (rel.equals("search")) &&
	    		 (href.contains("opensearch_desc.php"))) {
	    		try {
					openSearchURL = new URL(url, href);
				} catch (MalformedURLException e) {
					LOG.error("Wow, MediaWiki gave us back a mal-formed URL as the opensearch PHP value.", e);
					return apiURL;
				}
	    	}
	    	elem = it.next();
	    }
	    
	    if (openSearchURL == null) {
	    	throw new RuntimeException("uh, we failed to find the OpenSearch URL. This means we will not be able to find the MediaWiki API path for the feed.");
	    }
	    
	    // Okay, so we have the opensearch URL. Just replace the opensearch_desc.php with "api.php"
	    try {
			apiURL = new URL(openSearchURL, "api.php");
		} catch (MalformedURLException e) {
			LOG.error("If you hit this, it means Java cannot join two URL parts.", e);
			return apiURL;
		}
	    return apiURL;
	}

	public String calculateMediawikiPageRoot(String pageURI) {
		/* Watch me re-implement Python string's rsplit() method */
		int lastSlashIndex = StringUtils.lastIndexOf(pageURI, '/');
		int theFirstLetterOfThePageName = lastSlashIndex + 1;
		return pageURI.substring(0, theFirstLetterOfThePageName);
	}


}
