package org.creativecommons.learn.aggregate;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.creativecommons.learn.DEdConfiguration;
import org.creativecommons.learn.RdfStore;
import org.creativecommons.learn.RdfStoreFactory;
import org.creativecommons.learn.aggregate.feed.MediaWikiCategory;
import org.creativecommons.learn.aggregate.feed.OaiPmh;
import org.creativecommons.learn.aggregate.feed.Opml;
import org.creativecommons.learn.aggregate.feed.SyndFeedPoller;
import org.creativecommons.learn.oercloud.Feed;
import org.creativecommons.learn.oercloud.Resource;
import org.creativecommons.learn.plugin.MetadataRetrievers;
import org.mortbay.log.Log;

import com.sun.syndication.feed.module.DCModule;
import com.sun.syndication.feed.module.DCSubject;
import com.sun.syndication.feed.synd.SyndCategory;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.FeedException;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

public class FeedUpdater {

	private Feed feed;
	public static int howManyGETsSoFar;
	private static boolean pleaseCountGETs = false;
	private static Set<String> validFeedTypes;
	private MetadataRetrievers metadataRetrievers;

	public FeedUpdater(Feed feed) {
		this.feed = feed;
	}

	/*
	 * Public users of this method could get the object and 
	 * mutate it (i.e., change what feed types are valid),
	 * thereby changing the way FeedUpdate works. Protecting ourself
	 * from this strikes me as not worth the bother.
	 */
	public static Set<String> getValidFeedTypes() {
		if (validFeedTypes == null) {
			validFeedTypes = new HashSet<String>();
			validFeedTypes.add("oai-pmh");
			validFeedTypes.add("rss");
			validFeedTypes.add("mediawiki-category");
			validFeedTypes.add("opml");
		}
		return validFeedTypes;
	}
	
	public static boolean isFeedTypeValid(String feedType) {
		return getValidFeedTypes().contains(feedType);
	}
	
	public void update() throws IOException {
		// get the contents of the feed and emit events for each
		// FIXME: each what?

		RdfStore store = RdfStoreFactory.get().forProvenance(feed.getUri().toString());
		
		if (! isFeedTypeValid(feed.getFeedType())) {
			Logger.getLogger(Feed.class.getName()).warning(
					"Feed " + feed.getUri().toString() + " has unknown feed type of " +
					feed.getFeedType() + ". Skipping it during update.");
			return; // actually skip it.
		}

		// OPML
		if (feed.getFeedType().toLowerCase().equals("opml")) {
			new Opml().poll(feed);
		} else if (feed.getFeedType().toLowerCase().equals("oai-pmh")) {
			new OaiPmh().poll(feed);
		} else if (feed.getFeedType().toLowerCase().equals("rss")) {
			new SyndFeedPoller().poll(feed);
		} else if (feed.getFeedType().toLowerCase().equals("mediawiki-category")) {
			new MediaWikiCategory().poll(feed);
		} else {
			throw new IllegalArgumentException("Feed " + feed.getUri().toString() + " has invalid type, namely " + feed.getFeedType());
		}

        store.close();
	} // poll

	public static void startCountingGETs() {
		assert !pleaseCountGETs;
		howManyGETsSoFar = 0;
		pleaseCountGETs = true;
	}

	public static int getHowManyGETsSoFar() {
		return howManyGETsSoFar;
	}
}
