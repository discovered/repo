package org.creativecommons.learn.feed;
import java.util.Collection;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.creativecommons.learn.RdfStoreFactory;
import org.creativecommons.learn.oercloud.Curator;

public class ListCurators {
	/**
	 * Create an object that will allow us to parse the arguments
	 * passed to this class on the command line.
	 */
	@SuppressWarnings("static-access")
	private static Options getOptions() {
		Options options = new Options();

		Option help = new Option("help", "Print command line arguments");
		Option nullterminate = OptionBuilder.withArgName("nullterminate")
				.hasArg(false)
				.withDescription("Print feed URLs, one at a time, separated by a NUL byte.")
				.isRequired(false).create("nullterminate");
		options.addOption(help);
		options.addOption(nullterminate);
		return options;
	}

	/**
     * List feeds we're tracking
	 * @param args
	 */
	public static void main(String[] args) {
		// create the parser
		CommandLineParser parser = new GnuParser();
		CommandLine line = null;

		try {
			// parse the command line arguments
			line = parser.parse(getOptions(), args);
		} catch (ParseException exp) {
			// oops, something went wrong
			System.err.println("Parsing failed.  Reason: " + exp.getMessage());
			
			// exit with an exit code of 1
			System.exit(1);
		}

		if (line.hasOption("help")) {
			// automatically generate the help statement
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("listcurators", getOptions());

			System.exit(0);
		}

		
		Collection<Curator> curators = RdfStoreFactory.get().forDEd().load(Curator.class); 
			
		for (Curator c : curators) {
			if (line.hasOption("nullterminate")) {
				System.out.print(c.getUri().toString() + "\0");
			} else {
				System.out.println(c.getName() + " (" + c.getUrl() + ")");
			}
		}
		
	}

}
