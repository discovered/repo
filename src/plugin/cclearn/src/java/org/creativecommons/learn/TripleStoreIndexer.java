package org.creativecommons.learn;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.lucene.document.Field;
import org.apache.nutch.crawl.CrawlDatum;
import org.apache.nutch.crawl.Inlinks;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.indexer.lucene.LuceneWriter;
import org.apache.nutch.parse.Parse;
import org.creativecommons.learn.oercloud.Feed;
import org.creativecommons.learn.oercloud.Resource;

import thewebsemantic.NotFoundException;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.graph.impl.LiteralLabel;

import de.fuberlin.wiwiss.ng4j.Quad;

public class TripleStoreIndexer implements IndexingFilter {
        HashMap <String,String> DEFAULT_NAMESPACES = null;
        
	public Collection<String> getAllPossibleFieldNames() {
		HashSet<String> fieldNames = new HashSet<String>();

		// Get all the Rdfstores. Explode all the possible column names.
		// For each RdfStore, get all the predicates. For each predicate, create a
		// ProvenancePredicatePair, and add its .toFieldName() to a list. Return
		// the list.
		
		RdfStoreFactory everything = RdfStoreFactory.get();
		
		Iterator<Quad> allQuads = everything.findQuads(Node.ANY, Node.ANY, Node.ANY, Node.ANY);
		
		while(allQuads.hasNext()) {
			Quad q = allQuads.next();
			String fieldName = IndexFieldName.toFieldName(q);
			fieldNames.add(fieldName);
		}
		
		// Add field names that come from the site-specific field_names.xml configuration file.
		for (Entry<String, String> entry : customFieldConfiguration) {
			String key = entry.getKey();
			fieldNames.add(key);
		}

		// Add field name corresponding to non-provenance-bound fields
		fieldNames.add(Search.FEED_FIELD);
		fieldNames.add(Search.CURATOR_INDEX_FIELD);
		fieldNames.add(Search.ALL_CURATORS_INDEX_FIELD);

		return fieldNames;
	}

	public static final Log LOG = LogFactory.getLog(TripleStoreIndexer.class
			.getName());

	private Configuration conf;

	private Configuration customFieldConfiguration;

	public TripleStoreIndexer() {

		LOG.info("Created TripleStoreIndexer.");

		// initialize the set of default mappings
		DEFAULT_NAMESPACES = new HashMap<String, String>();
		DEFAULT_NAMESPACES.put(CCLEARN.getURI(), CCLEARN.getDefaultPrefix());
		DEFAULT_NAMESPACES.put("http://purl.org/dc/elements/1.1/", "dct");
		DEFAULT_NAMESPACES.put("http://purl.org/dc/terms/", "dct");
		DEFAULT_NAMESPACES.put("http://www.w3.org/1999/02/22-rdf-syntax-ns#",
				"rdf");

		this.customFieldConfiguration = new Configuration(false); // loadDefaults is off, so we get an empty one.
		this.customFieldConfiguration.addResource("discovered-search-prefixes.xml");

		System.out.println("TripleStoreIndexer has been constructed");
	}

	public Configuration getCustomFieldConfiguration() {
	    return this.customFieldConfiguration;
	}

	public void addField(String lucene_column_name) {
        LOG.info("Adding field " + lucene_column_name);
		LuceneWriter.addFieldOptions(lucene_column_name,
				LuceneWriter.STORE.YES, LuceneWriter.INDEX.UNTOKENIZED, conf);
	}

	@Override
	public void addIndexBackendOptions(Configuration conf) {
		for (String lucene_column_name : getAllPossibleFieldNames()) {
			this.addField(lucene_column_name);
		}

	} // addIndexBackendOptions
	
    /*
     * If there are no values for that field name we return null
     */
    public Collection<String> getValuesForCustomLuceneFieldName(String resourceURI, String fieldName) {
    	HashSet<String> values = new HashSet<String>();
    	/* First, figure out which RDF predicate URI this fieldName refers to. */
    	String predicateURI = this.customFieldConfiguration.get(fieldName);
    	if (predicateURI == null) {
    		LOG.warn("Yikes, you queried the IndexFilter for a custom field name that is not configured: " + fieldName);
    		return values;
    	}

        LOG.info("predicate URI: " + predicateURI);
        
        // Find all triples that refer to that predicate as refers to this resource
        RdfStoreFactory factory = RdfStoreFactory.get();
        Iterator<Quad> it = factory.findQuads( 
                Node.ANY, 
                Node.createURI(resourceURI),
                Node.createURI(predicateURI),
                Node.ANY);

        while (it.hasNext()) {
            Quad q = it.next();
            Triple statement = q.getTriple();
			Node node = statement.getObject();
			if (node.isLiteral()) {
				LiteralLabel literal = node.getLiteral();
				values.add(literal.toString());
			} else if (node.isURI()) {
				values.add(node.getURI().toString());
			} else {
				String asString = node.toString();
				LOG.warn("Weird, a node of an unusual type: " + asString);
				values.add(asString);
			}
        }

        return values;
    }

	@Override
	public NutchDocument filter(NutchDocument doc, Parse parse, Text url,
			CrawlDatum datum, Inlinks inlinks) throws IndexingException {
         // This method is called once per URL that Nutch is crawling, during
         // the "Indexing" phase
		
		String subjectURL = url.toString();
		return this.filter(doc, subjectURL);
	}
	
	public NutchDocument filter(NutchDocument doc, String subjectURL) {
		LOG.info("RdfStore: Begin indexing " + subjectURL);

		// Index all triples
		LOG.info("RdfStore: indexing all triples.");
		indexTriples(doc, subjectURL);
		
        // The discovered-search-prefixes.xml file configures, in a
        // site-specific way, what Lucene column names to use for certain
        // predicates. Here, we index those. FIXME: This is done in a
        // provenance-naive way.
        //
        // FIXME: customFieldConfiguration appears to be
        // containing much more than it should. We really only want to add the
        // DiscoverEd custom fields, not the Hadoop built-in "custom fields"
		for (Entry<String, String> entry : customFieldConfiguration) {
            LOG.info("I might add this to Lucene: " + entry.getKey() + ": " + entry.getValue());
			String fieldName = entry.getKey();
	        Collection<String> values = this.getValuesForCustomLuceneFieldName(
	                subjectURL, fieldName);
	        for (String value : values) {
                LOG.info("Adding to Lucene... " + fieldName + ", " + value);
	        	doc.add(fieldName, value);
	        }
		}

		// Follow special cases (curator)
		LOG.debug("RdfStore: indexing special cases.");
		LOG.warn("TripleStoreIndexer is about to try to index this URL: " + subjectURL);

        for (String provURI: RdfStoreFactory.get().getProvenancesThatKnowResourceWithThisURI(subjectURL)) {
            RdfStore store = RdfStoreFactory.get().forProvenance(provURI);
            try {
                Resource resource = store.loadDeep(Resource.class, subjectURL);
                this.indexSources(doc, resource);
            }
            catch (NotFoundException e) {
                // Silence this error.
            } 
            finally {
                store.close();
            }
        }

        LOG.info("TripleStoreIndexer: Calculating all curators string.");
        Resource resource = RdfStoreFactory.get().getReader().load(
        		Resource.class, subjectURL);
        String all_curators_string = resource.getAllCuratorURIsInCanonicalForm();
        doc.removeField(Search.ALL_CURATORS_INDEX_FIELD);
        doc.add(Search.ALL_CURATORS_INDEX_FIELD, all_curators_string);
        LOG.info("TripleStoreIndexer: Stored all curators for " + subjectURL + " as " + all_curators_string);

		// Return the document
		return doc;

	} // public Document filter
	
	private void indexTriples(NutchDocument doc, String subjectURL) {
		RdfStoreFactory factory = RdfStoreFactory.get();
		Iterator<Quad> it = factory.findQuads(
				Node.ANY, Node.createURI(subjectURL), Node.ANY, Node.ANY);
		
		// Index the triples
		while (it.hasNext()) {
			Quad q = it.next();
			this.indexProvenanceSpecificStatement(doc, q);
		}
	}

	private void indexSources(NutchDocument doc, Resource resource) {

		for (Feed source : resource.getSources()) {

			// add the feed URL to the resource 
			doc.add(Search.FEED_FIELD, source.getUri().toString());

			// if this feed has curator information attached, index it as well
			String curator_url = "";
			if (source.getCurator() != null) {
				curator_url = source.getCurator().getUri().toString();
			}

			// LuceneWriter.add(doc, curator);
			doc.add(Search.CURATOR_INDEX_FIELD, curator_url);
		}
	}

	private void indexProvenanceSpecificStatement(NutchDocument doc, Quad q) {
		// index a single statement
		Node objNode = q.getObject();
		String object = objNode.toString();

		// process the object...
		if (objNode.isLiteral()) {
			object = objNode.getLiteral().getValue().toString();
		}
		
		// index in a provenance-specific way:
		String fieldName = IndexFieldName.toFieldName(q);
		// ^ This is the same as a predicate with the provenance encoded into it
		LOG.debug("Adding to document (" + fieldName + ", " + object + ").");
		doc.add(fieldName, object);
	}

	public void setConf(Configuration conf) {
		this.conf = conf;
	}

	public Configuration getConf() {
		return this.conf;
	}

} // TripleStoreIndexer
