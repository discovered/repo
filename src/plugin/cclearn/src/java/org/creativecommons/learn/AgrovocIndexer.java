package org.creativecommons.learn;

import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.Text;
import org.apache.nutch.crawl.CrawlDatum;
import org.apache.nutch.crawl.Inlinks;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.indexer.lucene.LuceneWriter;
import org.apache.nutch.parse.Parse;
import org.creativecommons.learn.plugin.Agrovoc;

import com.hp.hpl.jena.graph.Node;

import de.fuberlin.wiwiss.ng4j.Quad;

public class AgrovocIndexer implements IndexingFilter {
	public static final Log LOG = LogFactory.getLog(AgrovocIndexer.class
			.getName());
	
	private final static Node DC_SUBJECT = Node.createURI("http://purl.org/dc/elements/1.1/subject");
	public final static String AGROVOC_PROVENANCE = "http://search.agshare.org/#agrovoc";

    public final static String AGROVOC_FIELD = IndexFieldName.makeCompleteFieldNameWithProvenance(
    			AGROVOC_PROVENANCE,
    			DC_SUBJECT.getURI());

	private Configuration conf;
	
	public AgrovocIndexer() {
		super();
		LOG.info("Created an AgrovocIndexer.");
	}

	@Override
	public void addIndexBackendOptions(Configuration conf) {
		LOG.info("Adding field for Agrovoc dct:subject backend");
			LuceneWriter.addFieldOptions(AGROVOC_FIELD,
					LuceneWriter.STORE.YES, LuceneWriter.INDEX.UNTOKENIZED, conf);
	} // addIndexBackendOptions

	@Override
	public NutchDocument filter(NutchDocument doc, Parse parse, Text url,
			CrawlDatum datum, Inlinks inlinks) throws IndexingException {
		// Find any dct:subject predicates from the Quad store for this Resource
		RdfStoreFactory everything = RdfStoreFactory.get();

		Iterator<Quad> allQuads = everything.findQuads(
				Node.ANY, // provenance
				Node.createURI(url.toString()), // resource 
				DC_SUBJECT,  // predicate
				Node.ANY); // object
		
		while(allQuads.hasNext()) {
			Quad quad = allQuads.next();
			String subject = quad.getObject().getLiteral().getValue().toString();
			for (String value: Agrovoc.getRelatedTermsFromAgrovoc(subject)) {
				doc.add(AGROVOC_FIELD, value);
			}
			for (String value: Agrovoc.getTranslations(subject)) {
				doc.add(AGROVOC_FIELD, value);
			}
		}

		return doc;
	}

	
	@Override
	public void setConf(Configuration conf) {
		this.conf = conf;
	}

	@Override
	public Configuration getConf() {
		return this.conf;
	}

}
